import users from "../models/user.js";
import tasks from "../models/todo.js";
import { v4 as uuidv4 } from "uuid";
import dotenv from "dotenv";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import jwtAuth from "../middlewears/jwtAuth.js";

dotenv.config();

export const createNewUser = async (req, res) => {
  const { email, password } = req.body;
  const user = await users.find({ email });
  if (user.length > 0) {
    return res.status(401).send("Account already exists");
  }
  const id = uuidv4();

  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(password, salt);

  try {
    const newUser = await users.create({
      id: id,
      email: email,
      password: hashedPassword,
    });

    return res.status(201).json("user created successfully");
  } catch (error) {
    return res.status(400).json({ message: "error adding new user" });
  }
};

export const logInUser = async (req, res) => {
  try {
    const { email, password, id } = req.body;
    const user = await users.findOne({ email });
    const hashedPassword = user ? user.password : null;
    const checkedPassword = bcrypt.compare(password, hashedPassword);

    if (!checkedPassword) {
      return response.json("failed to log in");
    }
    const task = await tasks.find({ userId: user.id });

    const token = jwtAuth(user);

    res.status(201).json(token);
  } catch (error) {
    res.status(400).json(`error message: ${error}`);
  }
};
