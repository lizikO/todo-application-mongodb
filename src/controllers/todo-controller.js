import tasks from "../models/todo.js";
import { v4 as uuidv4 } from "uuid";

export const getAllTasks = async (req, res) => {
  const { userId } = req.body;
  const data = await tasks.find(userId);
  return res.status(200).send(data);
};

export const createTask = async (req, res) => {
  const { todo, userId } = req.body;
  const id = uuidv4();
  try {
    const newTask = new tasks({
      todo,
      status: false,
      id: id,
      userId,
    });
    console.log(newTask);
    await newTask.save();
    res.status(201).json(newTask); // Send back the created task
  } catch (error) {
    res.status(400).json({ message: "Error adding task" });
  }
};

export const deleteTask = async (request, response) => {
  const { taskId } = request.params;
  try {
    const result = await tasks.deleteOne({ id: taskId });
    if (result.deletedCount === 1) {
      response.status(200).send("task was delated successfully");
    } else {
      response.status(404).send("task not found");
    }
  } catch (error) {
    response.satus(500).send("an error occured while delating the task");
  }
};

export const updateTaskTrue = async (request, response) => {
  const { taskId } = request.params;
  console.log(taskId);
  try {
    const result = await tasks.updateOne(
      { id: taskId },
      { $set: { status: true } }
    );
    if (result.matchedCount === 1) {
      response.status(200).send("Task was updated successfully");
    } else {
      response.status(404).send("Task not found");
    }
  } catch (error) {
    response.status(500).send("An error occurred while updating the Task");
  }
};
export const updateTaskFalse = async (request, response) => {
  const { taskId } = request.params;
  console.log(taskId);
  try {
    const result = await tasks.updateOne(
      { id: taskId },
      { $set: { status: false } }
    );
    if (result.matchedCount === 1) {
      response.status(200).send("Task was updated successfully");
    } else {
      response.status(404).send("Task not found");
    }
  } catch (error) {
    response.status(500).send("An error occurred while updating the Task");
  }
};
