import express from "express";
import {
  getAllTasks,
  createTask,
  updateTaskTrue,
  updateTaskFalse,
  deleteTask,
} from "../controllers/todo-controller.js";

const toDoRouter = express.Router();

toDoRouter.get("/todo", getAllTasks);
toDoRouter.post("/todo", createTask);
toDoRouter.put("/todo/:taskId", updateTaskTrue);
toDoRouter.delete("/todo/:taskId", deleteTask);
toDoRouter.get("/done", getAllTasks);
toDoRouter.put("/done/:taskId", updateTaskFalse);
toDoRouter.delete("/done/:taskId", deleteTask);

export default toDoRouter;
