import express from "express";
import { createNewUser, logInUser } from "../controllers/users-controller.js";

const userRouter = express.Router();
userRouter.post("/createUser", createNewUser);
userRouter.post("/logInUser", logInUser);

export default userRouter;
