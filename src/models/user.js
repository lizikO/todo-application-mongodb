import mongoose from "mongoose";

const { Schema } = mongoose;
const userSchema = new Schema({
  id: {
    type: Schema.Types.String,
    required: true,
  },
  email: {
    type: Schema.Types.String,
    required: true,
  },
  password: {
    type: Schema.Types.String,
    required: true,
  },
});

const users = mongoose.model("users", userSchema);

export default users;
