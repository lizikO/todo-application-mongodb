import mongoose from "mongoose";

const { Schema } = mongoose;

const taskSchema = new Schema({
  id: {
    type: Schema.Types.String,
    required: true,
  },
  todo: {
    type: Schema.Types.String,
    required: true,
  },
  status: {
    type: Schema.Types.Boolean,
    required: true,
  },
  userId: {
    type: Schema.Types.String,
    required: true,
  },
});

const tasks = mongoose.model("todos", taskSchema);

export default tasks;
