import bodyParser from "body-parser";
import express from "express";
import dotenv from "dotenv";
import cookieParser from "cookie-parser";
import cors from "cors";
import swaggerMiddleware from "./middlewears/swagger-middlewear.js";
import connectToMongo from "../src/config/mongo.js";
import toDoRouter from "./router/todo-router.js";
import userRouter from "./router/user-router.js";
const app = express();
dotenv.config();
connectToMongo();
app.use(cors());

app.use(bodyParser.json());
connectToMongo();
app.use(cookieParser());

app.use("/api", toDoRouter);
app.use("/api", userRouter);

app.use("/", ...swaggerMiddleware());
app.listen(3001);
